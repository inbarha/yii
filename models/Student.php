<?php

namespace app\models;
use yii\db\ActiveRecord;

class Student extends ActiveRecord
{

 public static function tableName(){
	 return 'student';
 }

	
	public static function getName($id){
		$student = self::findOne($id);
		isset($student)?
		$return = $student->name:
		$return = "No Student found with id $id";
		return $return;
	}


	public static function getStudent($id){
		$student = self::findOne($id);
		isset($student)?
		$return =$student:
		$return = "No Student found with id $id";
		return $return;
	}

	public static function getStudents(){
		$students = self::find()->all();
		return $students;
	}
 
 
 }
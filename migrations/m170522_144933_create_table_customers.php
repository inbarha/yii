<?php

use yii\db\Migration;

class m170522_144933_create_table_customers extends Migration
{
    public function up()
   
   {
  $this->createTable('customers', [
            'id' => $this->primaryKey(),
			'name' => $this->string()->notNull(),
			'email' => $this->string()->notNull(),
			
        ]);
    }
   

    public function down()
    {
        $this->dropTable('customers');
    }

   
}

<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Student;


class StudentController extends Controller
{
 
	public function actionView($id)
    {
		$student = Student::getStudent($id);
		return $this->render('view',['student' => $student]);
    }
	
	public function actionViewall()
	{
		$student = Student::getStudents();
		return $this->render('viewAll',['student' => $student]);
	}
}

View Student